﻿using TicTacToe.Enum;

namespace TicTacToe.Models
{
	public class TicTacToe
	{
		private readonly Dictionary<int, bool?> gameTable;
		private readonly int[][] winningCombinations;
		private readonly Random random;

		public TicTacToe()
		{
			gameTable = new Dictionary<int, bool?>()
			{
				{ 0, null },
				{ 1, null },
				{ 2, null },
				{ 3, null },
				{ 4, null },
				{ 5, null },
				{ 6, null },
				{ 7, null },
				{ 8, null },
			};
			winningCombinations =
			[
				[0, 1, 2],
				[0, 3, 6],
				[3, 4, 5],
				[6, 7, 8],
				[1, 4, 7],
				[2, 5, 8],
				[0, 4, 8],
				[2, 4, 6]
			];

			random = new Random(DateTimeOffset.Now.Second);
		}

		public bool SetCell(int index, bool value)
		{
			if (gameTable[index] is null)
			{
				gameTable[index] = value;
				return true;
			}

			return false;
		}

		public ResultGame FinishGame()
		{
			foreach (int[]? combination in winningCombinations)
			{
				bool? cell1 = gameTable[combination[0]];
				bool? cell2 = gameTable[combination[1]];
				bool? cell3 = gameTable[combination[2]];

				if (cell1 is not null && cell1 == cell2 && cell2 == cell3)
				{
					return cell1 is true ? ResultGame.FirstUserWin : ResultGame.SecondUserWin;
				}
			}

			if (gameTable.Values.All(value => value is not null))
			{
				return ResultGame.Draw;
			}

			return ResultGame.NextStep;
		}

		public int StepComputer()
		{
			List<KeyValuePair<int, bool?>> availableCell = gameTable.Where(i => i.Value is null).ToList();
			if (availableCell.Count is 0) return -1;

			if (availableCell.Count is 1)
			{
				KeyValuePair<int, bool?> cell = availableCell[0];
				gameTable[cell.Key] = false;
				return cell.Key;
			}

			int index = random.Next(0, availableCell.Count - 1);
			KeyValuePair<int, bool?> randomCell = availableCell[index];
			gameTable[randomCell.Key] = false;

			return randomCell.Key;
		}
	}
}
