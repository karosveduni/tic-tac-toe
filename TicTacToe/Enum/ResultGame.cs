﻿namespace TicTacToe.Enum
{
	public enum ResultGame
	{
		FirstUserWin,
		SecondUserWin,
		Draw,
		NextStep,
	}
}
