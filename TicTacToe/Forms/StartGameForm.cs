﻿using TicTacToe.Enum;

namespace TicTacToe.Forms
{
	public partial class StartGameForm : Form
	{
		private const string X = "X";
		private const string O = "O";
		private const string FirstUser = "First user's turn";

		private readonly string SecondUser;

		private readonly Action<Label> Game;

		/// <summary>
		/// True - X
		/// False - O
		/// </summary>
		private bool _step = true;
		private readonly bool _gameWithUser;
		private readonly Models.TicTacToe ticTacToe;
		private readonly Label[] labels;

		public StartGameForm() : this(true)
		{

		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="gameWithUser">True - game with user; False - game with computer</param>
		public StartGameForm(bool gameWithUser)
		{
			_gameWithUser = gameWithUser;
			ticTacToe = new Models.TicTacToe();
			Game = gameWithUser ? GameWithUser : GameWithComputer;
			SecondUser = gameWithUser ? "Second user's turn" : "The computer is walking";
			InitializeComponent();
			Text = FirstUser;
			if (!gameWithUser)
				labels = [label1, label2, label3, label4, label5, label6, label7, label8, label9];
		}

		private void Label1_Click(object sender, EventArgs e)
		{
			SetCell(0, label1);
		}

		private void Label2_Click(object sender, EventArgs e)
		{
			SetCell(1, label2);
		}

		private void Label3_Click(object sender, EventArgs e)
		{
			SetCell(2, label3);
		}

		private void Label4_Click(object sender, EventArgs e)
		{
			SetCell(3, label4);
		}

		private void Label5_Click(object sender, EventArgs e)
		{
			SetCell(4, label5);
		}

		private void Label6_Click(object sender, EventArgs e)
		{
			SetCell(5, label6);
		}

		private void Label7_Click(object sender, EventArgs e)
		{
			SetCell(6, label7);
		}

		private void Label8_Click(object sender, EventArgs e)
		{
			SetCell(7, label8);
		}

		private void Label9_Click(object sender, EventArgs e)
		{
			SetCell(8, label9);
		}

		private void SetCell(int index, Label label)
		{
			if (ticTacToe.SetCell(index, _step))
			{
				Game.Invoke(label);
			}
		}

		private string GetNameUserOrComputer() => _gameWithUser ? "Second user" : "Computer";

		private void GameWithUser(Label label)
		{
			label.Text = _step ? X : O;
			Text = !_step ? FirstUser : SecondUser;
			ResultGame result = FinishGame();
			if(result is not ResultGame.NextStep)
			{
				Close();
			}
			_step = !_step;
		}

		private void GameWithComputer(Label label)
		{
			label.Text = X;
			ResultGame result = FinishGame();

			if (result is ResultGame.NextStep)
			{
				int indexLabel = ticTacToe.StepComputer();

				if (indexLabel is not -1)
				{
					labels[indexLabel].Text = O;
					result = FinishGame();
				}
			}
			
			if(result is not ResultGame.NextStep)
			{
				Close();
			}
		}

		private ResultGame FinishGame()
		{
			ResultGame res = ticTacToe.FinishGame();
			switch (res)
			{
				case ResultGame.FirstUserWin:
					MessageBox.Show("First user is WIN.", "Finish game");
					break;
				case ResultGame.SecondUserWin:
					MessageBox.Show($"{GetNameUserOrComputer()} is WIN.", "Finish game");
					break;
				case ResultGame.Draw:
					MessageBox.Show("DRAW", "Finish game");
					break;
			}

			return res;
		}
	}
}
