﻿namespace TicTacToe.Forms
{
	partial class StartGameForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			tableLayoutPanel = new TableLayoutPanel();
			label1 = new Label();
			label2 = new Label();
			label3 = new Label();
			label4 = new Label();
			label5 = new Label();
			label6 = new Label();
			label7 = new Label();
			label8 = new Label();
			label9 = new Label();
			tableLayoutPanel.SuspendLayout();
			SuspendLayout();
			// 
			// tableLayoutPanel
			// 
			tableLayoutPanel.BackColor = Color.Black;
			tableLayoutPanel.ColumnCount = 5;
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33444F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 5F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.3344421F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 5F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33112F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
			tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
			tableLayoutPanel.Controls.Add(label1, 0, 0);
			tableLayoutPanel.Controls.Add(label2, 2, 0);
			tableLayoutPanel.Controls.Add(label3, 4, 0);
			tableLayoutPanel.Controls.Add(label4, 0, 2);
			tableLayoutPanel.Controls.Add(label5, 2, 2);
			tableLayoutPanel.Controls.Add(label6, 4, 2);
			tableLayoutPanel.Controls.Add(label7, 0, 4);
			tableLayoutPanel.Controls.Add(label8, 2, 4);
			tableLayoutPanel.Controls.Add(label9, 4, 4);
			tableLayoutPanel.Dock = DockStyle.Fill;
			tableLayoutPanel.Location = new Point(0, 0);
			tableLayoutPanel.Name = "tableLayoutPanel";
			tableLayoutPanel.RowCount = 5;
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 33.33444F));
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 5F));
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 33.334446F));
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 5F));
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 33.33112F));
			tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
			tableLayoutPanel.Size = new Size(977, 450);
			tableLayoutPanel.TabIndex = 0;
			// 
			// label1
			// 
			label1.BackColor = Color.White;
			label1.Dock = DockStyle.Fill;
			label1.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label1.Location = new Point(3, 0);
			label1.Name = "label1";
			label1.Size = new Size(316, 146);
			label1.TabIndex = 0;
			label1.TextAlign = ContentAlignment.MiddleCenter;
			label1.Click += Label1_Click;
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.BackColor = Color.White;
			label2.Dock = DockStyle.Fill;
			label2.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label2.Location = new Point(330, 0);
			label2.Name = "label2";
			label2.Size = new Size(316, 146);
			label2.TabIndex = 1;
			label2.TextAlign = ContentAlignment.MiddleCenter;
			label2.Click += Label2_Click;
			// 
			// label3
			// 
			label3.AutoSize = true;
			label3.BackColor = Color.White;
			label3.Dock = DockStyle.Fill;
			label3.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label3.Location = new Point(657, 0);
			label3.Name = "label3";
			label3.Size = new Size(317, 146);
			label3.TabIndex = 2;
			label3.TextAlign = ContentAlignment.MiddleCenter;
			label3.Click += Label3_Click;
			// 
			// label4
			// 
			label4.AutoSize = true;
			label4.BackColor = Color.White;
			label4.Dock = DockStyle.Fill;
			label4.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label4.Location = new Point(3, 151);
			label4.Name = "label4";
			label4.Size = new Size(316, 146);
			label4.TabIndex = 3;
			label4.TextAlign = ContentAlignment.MiddleCenter;
			label4.Click += Label4_Click;
			// 
			// label5
			// 
			label5.AutoSize = true;
			label5.BackColor = Color.White;
			label5.Dock = DockStyle.Fill;
			label5.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label5.Location = new Point(330, 151);
			label5.Name = "label5";
			label5.Size = new Size(316, 146);
			label5.TabIndex = 4;
			label5.TextAlign = ContentAlignment.MiddleCenter;
			label5.Click += Label5_Click;
			// 
			// label6
			// 
			label6.AutoSize = true;
			label6.BackColor = Color.White;
			label6.Dock = DockStyle.Fill;
			label6.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label6.Location = new Point(657, 151);
			label6.Name = "label6";
			label6.Size = new Size(317, 146);
			label6.TabIndex = 5;
			label6.TextAlign = ContentAlignment.MiddleCenter;
			label6.Click += Label6_Click;
			// 
			// label7
			// 
			label7.AutoSize = true;
			label7.BackColor = Color.White;
			label7.Dock = DockStyle.Fill;
			label7.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label7.Location = new Point(3, 302);
			label7.Name = "label7";
			label7.Size = new Size(316, 148);
			label7.TabIndex = 6;
			label7.TextAlign = ContentAlignment.MiddleCenter;
			label7.Click += Label7_Click;
			// 
			// label8
			// 
			label8.AutoSize = true;
			label8.BackColor = Color.White;
			label8.Dock = DockStyle.Fill;
			label8.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label8.Location = new Point(330, 302);
			label8.Name = "label8";
			label8.Size = new Size(316, 148);
			label8.TabIndex = 7;
			label8.TextAlign = ContentAlignment.MiddleCenter;
			label8.Click += Label8_Click;
			// 
			// label9
			// 
			label9.AutoSize = true;
			label9.BackColor = Color.White;
			label9.Dock = DockStyle.Fill;
			label9.Font = new Font("Segoe UI", 9F, FontStyle.Bold);
			label9.Location = new Point(657, 302);
			label9.Name = "label9";
			label9.Size = new Size(317, 148);
			label9.TabIndex = 8;
			label9.TextAlign = ContentAlignment.MiddleCenter;
			label9.Click += Label9_Click;
			// 
			// StartGameForm
			// 
			AutoScaleDimensions = new SizeF(8F, 20F);
			AutoScaleMode = AutoScaleMode.Font;
			ClientSize = new Size(977, 450);
			Controls.Add(tableLayoutPanel);
			Name = "StartGameForm";
			Text = "StartGame";
			tableLayoutPanel.ResumeLayout(false);
			tableLayoutPanel.PerformLayout();
			ResumeLayout(false);
		}

		#endregion

		private TableLayoutPanel tableLayoutPanel;
		private Label label1;
		private Label label2;
		private Label label3;
		private Label label4;
		private Label label5;
		private Label label6;
		private Label label7;
		private Label label8;
		private Label label9;
	}
}