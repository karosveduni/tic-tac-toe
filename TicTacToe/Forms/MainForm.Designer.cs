﻿namespace TicTacToe
{
	partial class MainForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			groupBox1 = new GroupBox();
			butExit = new Button();
			butAboutProgram = new Button();
			butStartGameWIthCom = new Button();
			butStartGameWithUser = new Button();
			groupBox1.SuspendLayout();
			SuspendLayout();
			// 
			// groupBox1
			// 
			groupBox1.Controls.Add(butExit);
			groupBox1.Controls.Add(butAboutProgram);
			groupBox1.Controls.Add(butStartGameWIthCom);
			groupBox1.Controls.Add(butStartGameWithUser);
			groupBox1.Dock = DockStyle.Fill;
			groupBox1.ForeColor = Color.Black;
			groupBox1.Location = new Point(0, 0);
			groupBox1.Name = "groupBox1";
			groupBox1.Size = new Size(278, 283);
			groupBox1.TabIndex = 0;
			groupBox1.TabStop = false;
			groupBox1.Text = "Main menu";
			// 
			// butExit
			// 
			butExit.BackColor = Color.White;
			butExit.FlatAppearance.BorderColor = Color.Red;
			butExit.FlatAppearance.BorderSize = 5;
			butExit.FlatStyle = FlatStyle.Flat;
			butExit.Font = new Font("Times New Roman", 12.2F);
			butExit.ForeColor = Color.Black;
			butExit.Location = new Point(60, 227);
			butExit.Name = "butExit";
			butExit.Size = new Size(170, 50);
			butExit.TabIndex = 0;
			butExit.Text = "Exit";
			butExit.UseVisualStyleBackColor = false;
			butExit.Click += ButExit_Click;
			// 
			// butAboutProgram
			// 
			butAboutProgram.BackColor = Color.Black;
			butAboutProgram.FlatAppearance.BorderColor = Color.Gold;
			butAboutProgram.FlatAppearance.BorderSize = 5;
			butAboutProgram.FlatStyle = FlatStyle.Flat;
			butAboutProgram.Font = new Font("Times New Roman", 12.2F);
			butAboutProgram.ForeColor = Color.White;
			butAboutProgram.Location = new Point(6, 140);
			butAboutProgram.Name = "butAboutProgram";
			butAboutProgram.Size = new Size(260, 50);
			butAboutProgram.TabIndex = 0;
			butAboutProgram.Text = "About program";
			butAboutProgram.UseVisualStyleBackColor = false;
			butAboutProgram.Click += ButAboutProgram_Click;
			// 
			// butStartGameWIthCom
			// 
			butStartGameWIthCom.BackColor = Color.Black;
			butStartGameWIthCom.FlatAppearance.BorderColor = Color.Gold;
			butStartGameWIthCom.FlatAppearance.BorderSize = 5;
			butStartGameWIthCom.FlatStyle = FlatStyle.Flat;
			butStartGameWIthCom.Font = new Font("Times New Roman", 12.2F);
			butStartGameWIthCom.ForeColor = Color.White;
			butStartGameWIthCom.Location = new Point(6, 82);
			butStartGameWIthCom.Name = "butStartGameWIthCom";
			butStartGameWIthCom.Size = new Size(260, 50);
			butStartGameWIthCom.TabIndex = 0;
			butStartGameWIthCom.Text = "Start Game With Computer";
			butStartGameWIthCom.UseVisualStyleBackColor = false;
			butStartGameWIthCom.Click += ButStartGameWIthCom_Click;
			// 
			// butStartGameWithUser
			// 
			butStartGameWithUser.BackColor = Color.Black;
			butStartGameWithUser.FlatAppearance.BorderColor = Color.Gold;
			butStartGameWithUser.FlatAppearance.BorderSize = 5;
			butStartGameWithUser.FlatStyle = FlatStyle.Flat;
			butStartGameWithUser.Font = new Font("Times New Roman", 12.2F);
			butStartGameWithUser.ForeColor = Color.White;
			butStartGameWithUser.Location = new Point(6, 26);
			butStartGameWithUser.Name = "butStartGameWithUser";
			butStartGameWithUser.Size = new Size(260, 50);
			butStartGameWithUser.TabIndex = 0;
			butStartGameWithUser.Text = "Start Game With User";
			butStartGameWithUser.UseVisualStyleBackColor = false;
			butStartGameWithUser.Click += ButStartGameWithUser_Click;
			// 
			// MainForm
			// 
			AutoScaleDimensions = new SizeF(9F, 19F);
			AutoScaleMode = AutoScaleMode.Font;
			BackColor = Color.White;
			ClientSize = new Size(278, 283);
			Controls.Add(groupBox1);
			Font = new Font("Times New Roman", 10.2F, FontStyle.Regular, GraphicsUnit.Point, 204);
			ForeColor = SystemColors.ControlLight;
			FormBorderStyle = FormBorderStyle.FixedToolWindow;
			Name = "MainForm";
			StartPosition = FormStartPosition.CenterScreen;
			Text = "Tic Tac Toe";
			groupBox1.ResumeLayout(false);
			ResumeLayout(false);
		}

		#endregion

		private GroupBox groupBox1;
		private Button butExit;
		private Button butAboutProgram;
		private Button butStartGameWIthCom;
		private Button butStartGameWithUser;
	}
}
