﻿using System.Diagnostics;

namespace TicTacToe.Forms
{
	public partial class AboutProgramForm : Form
	{
		private const string Url = "https://portfolio-website-illya-rybak.web.app/";
		public AboutProgramForm()
		{
			InitializeComponent();
		}

		private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			Process.Start(new ProcessStartInfo
			{
				FileName = Url,
				UseShellExecute = true,
			});
		}
	}
}
