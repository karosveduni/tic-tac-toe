using TicTacToe.Forms;

namespace TicTacToe
{
	public partial class MainForm : Form
	{
		private AboutProgramForm AboutProgramForm { get; set; } = new();

		public MainForm()
		{
			InitializeComponent();
		}

		private void ButStartGameWithUser_Click(object sender, EventArgs e)
		{
			using StartGameForm startGameForm = new();
			startGameForm.ShowDialog();
		}

		private void ButStartGameWIthCom_Click(object sender, EventArgs e)
		{
			using StartGameForm startGameForm = new(false);
			startGameForm.ShowDialog();
		}

		private void ButAboutProgram_Click(object sender, EventArgs e)
		{
			AboutProgramForm.ShowDialog();
		}

		private void ButExit_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
